package com.day3;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import com.day2.Base;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

// Appium Gesture Automation-1 using tapOptions instead of click
public class Example3 extends Base {

	public static void main(String[] args) throws MalformedURLException {
		
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// click on "Views"
		driver.findElementByAndroidUIAutomator("text(\"Views\")").click();;
		
		// tap on "Expandable Lists"
		TouchAction t = new TouchAction(driver);
		WebElement expandListWebElement = driver.findElementByXPath("//android.widget.TextView[@text='Expandable Lists']");
		t.tap(tapOptions().withElement(element(expandListWebElement))).perform();
			
		// click on "Custom Adapter"
		driver.findElementByAndroidUIAutomator("text(\"1. Custom Adapter\")").click();
		
		// long-press on People Names
		//	driver.findElementByAndroidUIAutomator("text(\"People Names\")").click();
		WebElement peoplesNameWebElement = driver.findElementByXPath("//android.widget.TextView[@text='People Names']");
		t.longPress(longPressOptions().withElement(element(peoplesNameWebElement))).perform();
		
		// conforming if the menu is displayed or not
		System.out.println(driver.findElementById("android:id/title").isDisplayed());
		System.out.println(driver.findElementById("android:id/title").getText());
		
	} 
}
