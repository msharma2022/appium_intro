package com.day3;

import com.day2.Base;
import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Example1 extends Base {

	

	// demo for AndroidUIAutomator
	public static void main(String[] args) throws MalformedURLException {
		
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//driver.findElementByAndroidUIAutomator("attribute("value")");
		driver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		
		// validate clickable feature for all options
		
		 List<AndroidElement> listOfClickableProperties = driver.findElementsByAndroidUIAutomator("new UiSelector().clickable(true)");
		 System.out.println("No. of cliackable elements:"+listOfClickableProperties.size());
		 
		 List<AndroidElement> listOfNonClickableProperties = driver.findElementsByAndroidUIAutomator("new UiSelector().clickable(false)");
		 System.out.println("No. of Non-cliackable elements:"+listOfNonClickableProperties.size());
	}
}
