package com.day3;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import com.day2.Base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

// Appium Gesture Automation-1
public class Example2 extends Base {

	public static void main(String[] args) throws MalformedURLException {
		
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// click on "Views"
		driver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		
		// click on "Expandable Lists"
		driver.findElementByAndroidUIAutomator("text(\"Expandable Lists\")").click();
		
		// click on "Custom Adapter"
		driver.findElementByAndroidUIAutomator("text(\"1. Custom Adapter\")").click();
		
		// click on People Names
		driver.findElementByAndroidUIAutomator("text(\"People Names\")").click();
		
	} 
}
