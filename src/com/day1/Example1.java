package com.day1;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class Example1 {

	// DesiredCapabilities introduction and first program to invoke an app
	
	public static void main(String[] args) throws MalformedURLException {
		AndroidDriver<AndroidElement> driver;

		DesiredCapabilities cap = new DesiredCapabilities();

		File appDir = new File("src");

		File app = new File(appDir, "ApiDemos-debug.apk");

		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "ManishEmulator");

		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");// new step
		
		System.out.println( app.getAbsolutePath());

		 cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
	//	cap.setCapability(MobileCapabilityType.APP,
	//			"C:\\Users\\sarca\\eclipse-workspace\\ccc\\src\\main\\java\\com\\ccc\\ApiDemos-debug.apk");

		driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	}
}
