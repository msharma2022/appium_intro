package com.day2;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Example1 extends Base {

	public static void main(String[] args) throws MalformedURLException {
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// most important locators: xpath, id, className, androidUIautomator
		
		/* xpath syntax
		 * //tagName[@attribute='value']		 * 
		 * 
		 */
	
		// now we will try to find the element 'Preference' by xpath
		driver.findElementByXPath("//android.widget.TextView[@text='Preference']").click();
		
	}
}
