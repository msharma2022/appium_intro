package com.day2;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Example3 extends Base {

	public static void main(String[] args) throws MalformedURLException {
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// most important locators: xpath, id, className, androidUIautomator
		
		/* xpath syntax
		 * //tagName[@attribute='value']		 * 
		 * 
		 */
	
		// now we will try to find the element 'Preference' by xpath
		driver.findElementByXPath("//android.widget.TextView[@text='Preference']").click();
		
		// now we want to click on 'Preference Dependencies' by xpath
		driver.findElementByXPath("//android.widget.TextView[@text='3. Preference dependencies']").click();
		
		// click on WiFi checkbox using xpath
		//driver.findElementByXPath("//android.widget.CheckBox[@resource-id='android:id/checkbox']").click();
		
		// click on WiFi checkbox using id
		driver.findElementById("android:id/checkbox").click();
		
		// click on WiFi Settings which must have got enabled by previous step
		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click();
		
		// Fill 'hello' in text field on pop-up 
		driver.findElementById("android:id/edit").sendKeys("hello");
		
		// now click ok
		driver.findElementsByClassName("android.widget.Button").get(1).click();
		
		
	}
}
