package com.day4;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import com.day2.Base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Example1 extends Base {

	public static void main(String[] args) throws MalformedURLException {
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// click on "Views"
		driver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		
		// click on "Date Widgets"
		driver.findElementByAndroidUIAutomator("text(\"Date Widgets\")").click();
		
		// click on "Inline"
		driver.findElementByAndroidUIAutomator("text(\"2. Inline\")").click();
		
		// click on 9 on the clock
		driver.findElementByXPath("//*[@content-desc='9']").click();
		
		// click on 45 on the clock
		driver.findElementByXPath("//*[@content-desc='45']").click();
		
		
	}
}
